<h3>Выполнение домашних работ AltexSoft - C#;</h3>

студент: Котов Юрий; <br>
email: mr.y.v.kotov@gamil.com;
<hr>

<b>Таски именуються в порядковом номере как указано в word файле.</b>

<h4> Week 1 </h4>
<ul>
<li>Task_2_2 - done</li>
<li>Task_2_3 - done</li>
<li>Task_2_4 - done</li> 
<li>Task_3_1 - done</li>
<li>Task_3_2 - done</li>
<li>Task_3_3 - done</li>
<li>Task_4_1 - done</li>
<li>Task_4_2 - done</li>
<li>Task_4_3 - done</li>
<li>Task_4_4 - done</li>
</ul>
<hr>

<h4>Week 2 </h4>
<ul>
<li>Task_5 - done</li>
<li>Task_6 - done</li>
<li>Task_8_1 - done</li>
<li>Task_8_2 - done</li>
</ul>

<h4>Week 3 </h4>
<ul>
<li>Task_9 - in progress</li>
<li>Task_10 - done</li> 
<li>Task_11 - done</li>
<li>Task_12 - done</li>
<li>Task_13 - done</li>
<li>Task_15 - done</li>
</ul>

<h4>Week 4 </h4>
<ul>
<li>Task_sql-1 - done</li>
<li>Task_sql-2 - done</li>
</ul>

<h4>Week 5 </h4>
<ul>
<li>Task_18 - done</li>
<li>Task_19 - done</li>
<li>Task_20 - done</li>
<li>Task_21 - done</li>
<li>Task_22 - done</li>
<li>Task_23 - done</li>
<li>Task_24 - done</li>
<li>Task_25 - done</li>
<li>Task_26 - done</li>
<li>Task_27 - done</li>
<li>LinqBegin_1 - done</li>
<li>LinqBegin_2 - done</li>
<li>LinqBegin_3 - done</li>
<li>LinqBegin_4 - done</li>
</ul>

<h4>Week 6</h4>
<ul>
<li>first-page - done</li>
</ul>