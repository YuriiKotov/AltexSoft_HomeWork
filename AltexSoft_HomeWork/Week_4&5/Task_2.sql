/*Task 1
������������ ���� ������ Northwind. �������� ������ �� ������� ���� ������� � ����� � ��������� [15,30)
*/
/*
SELECT pr.ProductName,pr.UnitPrice
FROM Products pr
where pr.UnitPrice >15 and pr.UnitPrice<30
order by pr.UnitPrice
go
*/

/*Task_2
������������ ���� ������ Northwind. �������� ������ �� ������� ���� ������� ������������ �� ��������� ����� ��. 
*/
/*
SELECT pr.ProductName
From Products pr
where pr.ProductName like 'c%'
go
*/

/*Task_3
 ������������ ���� ������ Northwind. �������� ������ �� ������� ���� ������� ������� � �������� ������ ��������� ����� �b�. (empty)
*/
/*
SELECT pr.ProductName
From Products pr
where pr.ProductName like '_b%'

go
*/

/*Task 4 
������������ ���� ������ Northwind. �������� ������ �� ������� ���� �������, � �������� ������� ����������� ������������������ �eso�. 
*/
/*
SELECT pr.ProductName
From Products pr
where pr.ProductName like '%eso%'
go
*/
/*Task 5
 ������������ ���� ������ Northwind. �������� ������ �� ������� ���� �������, �������� ������� ������������� �� �es�.
 */
 /*
 SELECT pr.ProductName
From Products pr
where pr.ProductName like '%es'
go
*/
/*Task 6 and 7 adn 8 
������������ ���� ������ Northwind. �������� ������ �� ������� ������� � ��������. (����������� � ����� �����, ����������, ���� �������) 
& (����������� � ����� �����, ����������, ���� �������, ���� �������, ����������)
& ��������� ������������ �� ������������ ������, ������ ����������.  
*/
/*
 SELECT pr.ProductName, od.Quantity, od.UnitPrice, o.OrderDate, c.CompanyName
From Products pr
join [Order Details] od on pr.ProductID = od.ProductID
join Orders o on od.OrderID = o.OrderID
join Customers c on o.CustomerID = c.CustomerID
Order by pr.ProductName
go
*/

/*Task 9
������������ ���� ������ Northwind. �������� ������ �� ������� ������� � ��������, �� 1997 ���. 
(����������� � ����� �����, ����������, ���� �������, ���� �������). ��������� ������������ �� ����, �������� ���������� */
/*
 SELECT pr.ProductName, od.Quantity, od.UnitPrice, o.OrderDate
From Products pr
join [Order Details] od on pr.ProductID = od.ProductID
join Orders o on od.OrderID = o.OrderID
join Customers c on o.CustomerID = c.CustomerID
where o.OrderDate > '19970101' and o.OrderDate<'19980101'
Order by od.UnitPrice  DESC
go
*/

/*Task 10
 �������� ������ �� ������� ������� � ��������, ��������� �� 1997 ����. 
 (����������� � ����� �����, ����������, ���� �������, ���� �������). ��������� ������������ �� ����, �������� ����������. 
 */
 /*
  SELECT pr.ProductName, od.Quantity, od.UnitPrice, o.OrderDate
From Products pr
join [Order Details] od on pr.ProductID = od.ProductID
join Orders o on od.OrderID = o.OrderID
join Customers c on o.CustomerID = c.CustomerID
where o.OrderDate < '19970101'
Order by od.UnitPrice  DESC
go
*/

/*Task 11
 �������� ������ �� ������� ������ 7 ������� � ��������. 
 (����������� � ����� �����, �����, ���� �������, ����������). ��������� ������������ �� ����, ������ ����������*/
 /*
  SELECT TOP (7) pr.ProductName, od.Quantity, od.UnitPrice, o.OrderDate, c.CompanyName
From Products pr
join [Order Details] od on pr.ProductID = od.ProductID
join Orders o on od.OrderID = o.OrderID
join Customers c on o.CustomerID = c.CustomerID
Order by od.UnitPrice
go
*/

/*Task 12 
�������� ������ �� ������� ���� ������� � ��������� �������� ������� ������. 
(����������� � ����� �����, �����). ��������� ������������ �� �����, �������� ����������.*/
/*
SELECT pr.ProductName, SUM((Od.UnitPrice - Od.UnitPrice * Od.Discount) * Od.Quantity) as summary
FROM Products pr
join [Order Details] od on pr.ProductID = od.ProductID
join Orders o on od.OrderID = o.OrderID
GROUP BY Pr.ProductName
Order by summary DESC
 go
 */

 /*Task 13 
 �������� ������ �� ������� ���� ������� � ���������� ������ ������� ������. 
 (����������� � �����, ����������). ��������� ������������ �� �����, �������� ����������.
 */
 /*
SELECT pr.ProductName,SUM (od.Quantity) as summary
FROM Products pr
join [Order Details] od on pr.ProductID = od.ProductID
join Orders o on od.OrderID = o.OrderID
GROUP BY Pr.ProductName
Order by summary DESC
 go
 */

 /*Task 14
 �������� ������ �� ������� ���� ������� � ��������� �������� ������� ������ �� 1997 ���. 
 (����������� � ����� �����, �����). ��������� ������������ �� �����, �������� ����������.
 */
 /*
 SELECT pr.ProductName, SUM((Od.UnitPrice - Od.UnitPrice * Od.Discount) * Od.Quantity) as summary
FROM Products pr
join [Order Details] od on pr.ProductID = od.ProductID
join Orders o on od.OrderID = o.OrderID
where o.OrderDate > '19970101' and o.OrderDate<'19980101'
GROUP BY Pr.ProductName
Order by summary DESC
go
*/

/*Task 15
�������� ������ �� ������� ������ 8 ������� � ��������� �������� ������� ������ �� 1997 ���.
(����������� � ����� �����, �����). ��������� ������������ �� �����, �������� ����������. 
*/
/*
 SELECT top(8) pr.ProductName, SUM((Od.UnitPrice - Od.UnitPrice * Od.Discount) * Od.Quantity) as summary
FROM Products pr
join [Order Details] od on pr.ProductID = od.ProductID
join Orders o on od.OrderID = o.OrderID
where o.OrderDate > '19970101' and o.OrderDate<'19980101'
GROUP BY Pr.ProductName
Order by summary DESC
go
*/

/*Task 16 
�������� ������ �� ������� ���� ������� � ��������� �������� ������� ����������, �� ����� ������� 500. 
 (����������� � ����������, �����). ��������� ������������ �� �����, �������� ����������.*/
 /*
  SELECT c.CompanyName, SUM((Od.UnitPrice - Od.UnitPrice * Od.Discount) * Od.Quantity) as summary
FROM Products pr
join [Order Details] od on pr.ProductID = od.ProductID
join Orders o on od.OrderID = o.OrderID
join Customers c on o.CustomerID = c.CustomerID
where ((Od.UnitPrice - Od.UnitPrice * Od.Discount) * Od.Quantity) > 500
GROUP BY c.CompanyName
Order by summary DESC
go
*/

/*Task 17 
 �������� ������ �� ������� ���� ������� � ��������� �������� ������� ����������, �� ����� ������� 1 000. 
 (����������� � ����������, �����). ��������� ������������ �� �����, �������� ����������. */
 /*
   SELECT c.CompanyName, SUM((Od.UnitPrice - Od.UnitPrice * Od.Discount) * Od.Quantity) as summary
FROM Products pr
join [Order Details] od on pr.ProductID = od.ProductID
join Orders o on od.OrderID = o.OrderID
join Customers c on o.CustomerID = c.CustomerID
where ((Od.UnitPrice - Od.UnitPrice * Od.Discount) * Od.Quantity) < '1000'
GROUP BY c.CompanyName
Order by summary DESC
 go
 */