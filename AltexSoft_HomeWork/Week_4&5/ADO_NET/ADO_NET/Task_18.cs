﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO_NET
{
    class Task_18
    {
        public void Print()
        {
            var connection =
               @"Data Source=.\SQLEXPRESS;Initial Catalog = Northwind;Integrated Security=true;Pooling = true";

            var connect = new SqlConnection(connection);

            connect.Open();

            var cmd =
                new SqlCommand(
                    "SELECT p.ProductName, SUM(od.Quantity) AS SummCountSales" +
                    " FROM Products p" +
                    " JOIN [Order Details] od ON p.ProductID=od.ProductID" +
                    " WHERE od.Quantity > 10" +
                    " GROUP BY p.ProductName" +
                    " ORDER BY SUM(od.Quantity) DESC",
                    connect);

            var read = cmd.ExecuteReader();

            while (read.Read())
            {
                Console.WriteLine("");
                for (var i = 0; i < read.FieldCount; i++)
                {
                    Console.WriteLine("{0} - {1}", read.GetName(i), read[i]);
                }
                Console.WriteLine(new string('_', 40));
            }
            Console.ReadLine();
            connect.Close();
        }

    }
}
