﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace ADO_NET
{
    internal class Task_22
    {
        public void PrintView()
        {
            var connection =
                @"Data Source=.\SQLEXPRESS;Initial Catalog = Northwind;Integrated Security=true;Pooling = true";
            var connect = new SqlConnection(connection);

            /* connect.Open();
 
             var cmd = new SqlCommand("CREATE VIEW Task_22 WITH ENCRYPTION AS " +
                                      "SELECT p.ProductName, CAST((CAST(od.UnitPrice as int) - CAST(od.UnitPrice as int) * CAST(od.Discount as int))" +
                                      " * od.Quantity as decimal) as Summary, ord.OrderDate " +
                                      "From Orders ord " +
                                      "Join [Order Details] od ON ord.OrderID = od.OrderID " +
                                      " Join Products p ON  od.ProductID = p.ProductID", connect);
 
             cmd.ExecuteReader();
             connect.Close();
             */
            var adapter = new SqlDataAdapter("SELECT TOP(7) * FROM Task_22", connection);

            var northwind = new DataTable();

            adapter.Fill(northwind);

            var view = northwind.DefaultView;
            view.Sort = "Summary";


            PrintTableOrView(view, "Task 22");
            Console.ReadLine();
        }


        private static void PrintTableOrView(DataView view, string label)
        {
            Console.WriteLine(label);
            var table = view.Table;
            foreach (DataRowView rowView in view)
            {
                var sw = new StringWriter();
                Console.WriteLine(new string('_', 100));

                foreach (DataColumn col in table.Columns)
                    sw.Write("\n{0} : {1}", col, rowView[col.ColumnName]);

                var output = sw.ToString();
                Console.WriteLine(output);
                Console.WriteLine(new string('_', 100));
            }
        }
    }
}