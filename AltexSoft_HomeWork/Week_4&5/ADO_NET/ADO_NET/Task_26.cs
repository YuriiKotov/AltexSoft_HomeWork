﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace ADO_NET
{
    internal class Task_26
    {
        public void PrintView()
        {
            var connection =
                @"Data Source=.\SQLEXPRESS;Initial Catalog = Northwind;Integrated Security=true;Pooling = true";
            var connect = new SqlConnection(connection);

            connect.Open();

            var cmd = new SqlCommand("CREATE VIEW Task_26 AS " +
                                     "SELECT p.ProductName, SUM(od.Quantity * (od.UnitPrice-od.UnitPrice *od.Discount)) as summary " +
                                     "FROM [Order Details] od " +
                                     "join Orders o ON od.OrderID = o.OrderID " +
                                     "Join Products p ON  od.ProductID = p.ProductID " +
                                     "WHERE o.OrderDate < '19980101' and o.OrderDate > '19970101' " +
                                     "GROUP BY p.ProductName", connect);

            cmd.ExecuteReader();
            connect.Close();

            var adapter = new SqlDataAdapter("SELECT Top 8 * FROM Task_26", connection);

            var northwind = new DataTable();

            adapter.Fill(northwind);

            var view = northwind.DefaultView;
            view.Sort = "summary DESC";


            PrintTableOrView(view, "Task 26");
            Console.ReadLine();
        }


        private static void PrintTableOrView(DataView view, string label)
        {
            Console.WriteLine(label);
            var table = view.Table;
            foreach (DataRowView rowView in view)
            {
                var sw = new StringWriter();
                Console.WriteLine(new string('_', 100));

                foreach (DataColumn col in table.Columns)
                    sw.Write("\n{0} : {1}", col, rowView[col.ColumnName]);

                var output = sw.ToString();
                Console.WriteLine(output);
                Console.WriteLine(new string('_', 100));
            }
        }
    }
}