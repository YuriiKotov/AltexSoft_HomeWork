﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO_NET
{
    class Task_20
    {
        public void Print()
        {
            var connection =
                @"Data Source=.\SQLEXPRESS;Initial Catalog = Northwind;Integrated Security=true;Pooling = true";

            var connect = new SqlConnection(connection);

            connect.Open();

            var cmd =
                new SqlCommand(
                    "SELECT cus.ContactName, SUM(od.Quantity) AS Amount" +
                    " FROM Orders ord" +
                    " JOIN Customers cus ON ord.CustomerID = cus.CustomerID" +
                    " JOIN [Order Details] od ON ord.OrderID = od.OrderID" +
                    " WHERE cus.ContactName like '%q%'" +
                    " GROUP BY cus.ContactName" +
                    " Order By SUM(od.Quantity)",
                    connect);

            var read = cmd.ExecuteReader();


            while (read.Read())
            {
                Console.WriteLine("");
                for (var i = 0; i < read.FieldCount; i++)
                {
                    Console.WriteLine("{0} - {1}", read.GetName(i), read[i]);
                }
                Console.WriteLine(new string('_', 40));
            }
            Console.ReadLine();
            connect.Close();
        }
    }
}
