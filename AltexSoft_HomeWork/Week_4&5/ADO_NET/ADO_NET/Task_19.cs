﻿using System;
using System.Data.SqlClient;

namespace ADO_NET
{
    internal class Task_19
    {
        public void Print()
        {
            var connection =
                @"Data Source=.\SQLEXPRESS;Initial Catalog = Northwind;Integrated Security=true;Pooling = true";

            var connect = new SqlConnection(connection);

            connect.Open();

            var cmd =
                new SqlCommand(
                    "SELECT cus.CompanyName, SUM(od.Quantity) AS Amount" +
                    " FROM Orders ord" +
                    " JOIN Customers cus ON ord.CustomerID = cus.CustomerID JOIN [Order Details] od ON ord.OrderID = od.OrderID" +
                    " WHERE od.Quantity < 5 GROUP BY cus.CompanyName ORDER BY SUM(od.Quantity)",
                    connect);

            var read = cmd.ExecuteReader();


            while (read.Read())
            {
                Console.WriteLine("");
                for (var i = 0; i < read.FieldCount; i++)
                {
                    Console.WriteLine("{0} - {1}", read.GetName(i), read[i]);
                }
                Console.WriteLine(new string('_', 40));
            }
            Console.ReadLine();
            connect.Close();
        }
    }
}