﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace ADO_NET
{
    internal class Task_27
    {
        public void PrintView()
        {
            var connection =
                @"Data Source=.\SQLEXPRESS;Initial Catalog = Northwind;Integrated Security=true;Pooling = true";
            var connect = new SqlConnection(connection);

            connect.Open();
           

            var cmd = new SqlCommand("CREATE VIEW Task_27 AS " +
                                     "SELECT cus.CompanyName, SUM(od.Quantity * (od.UnitPrice-od.UnitPrice *od.Discount)) as Summary " +
                                     "FROM Customers cus " +
                                     "Join Orders o ON cus.CustomerID = o.CustomerID " +
                                     "join [Order Details] od ON o.OrderID = od.OrderID " +
                                     "Group by cus.CompanyName " +
                                     "HAVING SUM(od.Quantity * (od.UnitPrice-od.UnitPrice *od.Discount)) > 500", connect);

            
            cmd.ExecuteReader();
            connect.Close();

            var adapter = new SqlDataAdapter("SELECT * FROM Task_27", connection);

            var northwind = new DataTable();

            adapter.Fill(northwind);

            var view = northwind.DefaultView;
            view.Sort = "Summary DESC";


            PrintTableOrView(view, "Task 27");
            Console.ReadLine();
        }


        private static void PrintTableOrView(DataView view, string label)
        {
            Console.WriteLine(label);
            var table = view.Table;
            foreach (DataRowView rowView in view)
            {
                var sw = new StringWriter();
                Console.WriteLine(new string('_', 100));

                foreach (DataColumn col in table.Columns)
                    sw.Write("\n{0} : {1}", col, rowView[col.ColumnName]);

                var output = sw.ToString();
                Console.WriteLine(output);
                Console.WriteLine(new string('_', 100));
            }
        }
    }
}
