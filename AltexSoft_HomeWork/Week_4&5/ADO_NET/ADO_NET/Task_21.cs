﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace ADO_NET
{
    internal class Task_21
    {
        public void Print()
        {
            var connection =
                @"Data Source=.\SQLEXPRESS;Initial Catalog = Northwind;Integrated Security=true;Pooling = true";

            var connect = new SqlConnection(connection);

            connect.Open();

            var cmd = new SqlCommand(
                "SELECT ProductName, Quantity, UnitPrice, OrderDate" +
                " FROM Invoices" +
                " WHERE OrderDate < '19970101'" +
                " ORDER BY UnitPrice DESC"
                , connect
            );
            var read = cmd.ExecuteReader();

            while (read.Read())
            {
                Console.WriteLine("");
                for (var i = 0; i < read.FieldCount; i++)
                    Console.WriteLine("{0} - {1}", read.GetName(i), read[i]);
                Console.WriteLine(new string('_', 40));
            }
            Console.ReadLine();
            connect.Close();
        }

        public void PrintView()
        {
            var connection =
                @"Data Source=.\SQLEXPRESS;Initial Catalog = Northwind;Integrated Security=true;Pooling = true";

            var adapter = new SqlDataAdapter(
                "SELECT p.ProductName, od.Quantity,od.UnitPrice, ord.OrderDate" +
                " From Orders ord" +
                " Join [Order Details] od ON ord.OrderID = od.OrderID" +
                " Join Products p ON  od.ProductID = p.ProductID" +
                " Where ord.OrderDate < '19970101'"
                , connection);

            /* SqlDataAdapter adapter = new SqlDataAdapter(
                 "SELECT *" +
                 " From Orders ord" +
                 " Join [Order Details] od ON ord.OrderID = od.OrderID" +
                 " Join Products p ON  od.ProductID = p.ProductID"
                 , connection);
                 */
            var table = new DataTable();
            adapter.Fill(table);

            DataView view = table.DefaultView;
            view.Sort = "UnitPrice DESC";

            PrintTableOrView(view, "Some lable");
            Console.ReadLine();
        }

        private static void PrintTableOrView(DataView view, string label)
        {
            Console.WriteLine(label);
            var table = view.Table;
            foreach (DataRowView rowView in view)
            {
                var sw = new StringWriter();
                Console.WriteLine(new string('_', 100));

                foreach (DataColumn col in table.Columns)
                    sw.Write("\n{0} : {1}", col, rowView[col.ColumnName]);

                var output = sw.ToString();
                Console.WriteLine(output);
                Console.WriteLine(new string('_', 100));
            }
        }

        public void AnotherPrint()
        {
            var connection =
                @"Data Source=.\SQLEXPRESS;Initial Catalog = Northwind;Integrated Security=true;Pooling = true";

            var querySelect = "SELECT * FROM Orders; SELECT * FROM [Order Details]; SELECT * FROM Products";

            DataSet northwind = new DataSet("Northwind");

            var adapter = new SqlDataAdapter(querySelect, connection);
            adapter.Fill(northwind);

            DataTable productTable = northwind.Tables["Products"];
            DataTable orderDetailsTable = northwind.Tables["[Order Details]"];
            DataTable ordersTable = northwind.Tables["Orders"];

           /* DataRelation newRelation = new DataRelation("Pr_Od",
            productTable.Columns["ProductID"],
            orderDetailsTable.Columns["ProductID"],
            true);

            northwind.Relations.Add(newRelation);*/
            
            DataView newView = new DataView();
            newView.Table = productTable;
            newView.Table = ordersTable;

            foreach (DataRowView someRows in newView)
            {
                Console.WriteLine(someRows["ProductName"]);
            }
            Console.ReadKey();
        }
    }
}