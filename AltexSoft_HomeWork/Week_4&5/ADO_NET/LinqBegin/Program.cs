﻿using System;
using System.Linq;

namespace LinqBegin
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
/*
                        Console.WriteLine("Linq - 1");
                        var array = new[] {1, 4, -1, 5, -7};
                        var reslinq = array.Where(a => a > 0).Take(1).Union(array.Reverse().Where(a => a < 0).Take(1));
                        foreach (var i in reslinq)
                            Console.WriteLine(i);
            
                        Console.WriteLine("\n\n");
            
            
                        Console.WriteLine("Linq - 2");
                        //Выполнять пока не вернет 0
                        int someN;
                        do
                        {
                            Console.WriteLine("Введите цыфру");
                            Console.WriteLine("может 4?");
                            someN = Convert.ToInt32(Console.ReadLine());
                            var intArray = new int[10] {1, 8, 11, 50, -90, 31, 2, 7, 0, 9};
                            var result21 = intArray.Where(a => ((a - someN)%10 == 0) && (a > 0)).FirstOrDefault();
                            Console.WriteLine(result21);
                        } while (someN != 4);
            
                        Console.WriteLine("\n\n");
                        */
            /*
            Console.WriteLine("Linq - 3");
   
   
            var l = 4;
            var stringArray31 = new string[5] {"abcde", "fjklm", "nrst", "uxvw", "01234"};
            var stringArray32 = new string[5] {"qwe", "1234", "qw", "qwertyu", "qwaszx"};
            var result31 =
                stringArray31.FirstOrDefault(s => (s.Length == l) && s.Substring(0, 1).Intersect("0123456789").Any()) ??
                "Not found";
            var result32 =
                stringArray32.FirstOrDefault(s => (s.Length == l) && s.Substring(0, 1).Intersect("0123456789").Any()) ??
                "Not found";
            Console.WriteLine(result31);
            Console.WriteLine(result32);
            Console.ReadKey();
            */
            /*
            Console.WriteLine("Linq - 4");

            var ch = 'c';
            var first = new string[3] { "sdf", "dsfff", "10"};
            var second = new string[3] { "adsa", "sdfff", "c"};
            var third = new string[3] {"qwe", "fdss", "cc2"};
           try
            {
                var resFirst = first.Where(s => s.Last() == ch);
                var enumerable1 = resFirst as string[] ?? resFirst.ToArray();
                if (!enumerable1.Any()) Console.WriteLine("");
                else if (enumerable1.Count() == 1) Console.WriteLine(enumerable1.ElementAt(0));
                else throw new Exception();

                var resSecond = second.Where(s => s.Last() == ch);
                var enumerable2 = resSecond as string[] ?? resSecond.ToArray();
                if (!enumerable2.Any()) Console.WriteLine("");
                else if (enumerable2.Count() == 1) Console.WriteLine(enumerable2.ElementAt(0));
                else throw new Exception();

                var resThird = third.Where(s => s.Last() == ch);
                var enumerable3 = resThird as string[] ?? resThird.ToArray();
                if (!enumerable3.Any()) Console.WriteLine("");
                else if (enumerable3.Count() == 1) Console.WriteLine(enumerable3.ElementAt(0));
                else throw new Exception();
            }
            catch
            {
                Console.WriteLine("Something Wrong!!");
            }*/
            /*
            Console.WriteLine("Linq - 5");

            char C = Convert.ToChar("c");
            string someStirng = "chymic carbonic cabinetmaker cantankerousness catastrophically";

            int count = 0;

            foreach (var i in someStirng.Split(' '))
            {
                if (i.LastIndexOf(C) != i.Length - 1 || (i[0] != C || i.Length <= 1)) continue;
                count++;
            }
            Console.WriteLine(count);
            */
            /*
            Console.WriteLine("Linq - 6");

            var cauntSumLeng = "actualizations acupunctures geophysicists generousness";
            var count = 0;

            var sum = cauntSumLeng.Split().Sum(i => i.Length);
            count += sum;
            Console.WriteLine("Сумма всех символов в строке - {0}", count);
            */

            Console.WriteLine("Linq - 7");


            Console.ReadKey();
        }
    }
}