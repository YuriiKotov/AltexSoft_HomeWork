﻿using System.Collections;
using System.Collections.Generic;

namespace Task_5_1
{
    public class Persons : IEnumerable<Student>, IEnumerator<Student>
    {
        private readonly List<Student> _students = new List<Student>();

        private int _position = -1;


        public IEnumerator<Student> GetEnumerator()
        {
            foreach (var student in _students)
                yield return student;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Dispose()
        {
            Reset();
        }

        public bool MoveNext()
        {
            if (_position >= _students.Count - 1) return false;
            _position++;
            return true;
        }

        public void Reset()
        {
            _position = -1;
        }

        public Student Current => _students[_position];

        object IEnumerator.Current => Current;

        public void Add(Student student )
        {
            _students.Add(student);
        }

    }
}