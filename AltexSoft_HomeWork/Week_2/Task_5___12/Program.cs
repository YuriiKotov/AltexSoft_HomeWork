﻿using System;
using System.Collections.Generic;

namespace Task_5_1
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine(
                "-----------------------------------------Task 1 (Hierarchy)-------------------------------------");
            Teacher[] teachers =
            {
                new Teacher("Albus Percival Wulfric Brian Dumbledore", 1),
                new Teacher("Filius Flitwick", 2),
                new Teacher("Rubeus Hagrid", 3)
            };
            var students = new List<Student>
            {
                new Student("Harry Potter", 4, 1),
                new Student("Hermiona Granger", 4, 2),
                new Student("Ronald Wezle", 4, 3),
                new Student("Luna Lavgud", 3, 3),
                new Student("Drako Malphoi", 4, 1),
                new Student("Neville Longbottom", 4, 3),
                new Student("Cedrik Diggory", 6, 2),
                new Student("Chou Chang", 6, 2),
                new Student("Victor Cram", 5, 1),
                new Student("Dafna Gringras", 1, 2),
                new Student("Kolin Krivi", 2, 2),
                new Student("Oliver Wood", 6, 2),
                new Student("Sam Krivi", 1, 2)
            };


            Teacher.PrintStudents(students.ToArray(), teachers);

            Console.WriteLine(
                "-----------------------------------------Task 3 (Random)-----------------------------------------");

            Console.WriteLine("\n_____________Winner cup of fire_____________\n");
            Student.RandomStudent(students.ToArray());
            Console.WriteLine("\n___________________Curator__________________\n");
            Teacher.RandomProff(teachers);

            Console.WriteLine(
                "-----------------------------------------Task 2 (ToString)-------------------------------------------");
            Person.TestToString(teachers, students.ToArray());

            Console.WriteLine(
                "------------------------------------------Task 2 (Equals)--------------------------------------------");
            Person.TestEquals(teachers, students);

            Console.WriteLine(
                "----------------------------------------Task 2 (GetHashCode)-----------------------------------------");
            Person.TestGetHashCode(students, teachers);

            Console.WriteLine(
                "-------------------------------------Task 4 (counter + up Cours)-------------------------------------");
            Person.GetType(students.ToArray(), teachers);

            Console.WriteLine(
                "---------------------------------------------Task 5 (Clone)------------------------------------------");
            var first = new Person("Tom", "Jarry");
            var second = (Person) first.Clone();
            Console.WriteLine("Клон: " + second.StudentName);

            Console.WriteLine(
                "---------------------------------------------Task 6 (Parents)----------------------------------------");
            Student.Parents(students[1]);

            Console.WriteLine(
                "---------------------------------------------Task 12_1 (IPrintable)----------------------------------------");


            var p = new Person("Sally", null);
            p.Print();
            var p1 = (IPrintable) p;
            p1.Print();

            Console.WriteLine(
                "---------------------------------------------Task 12_2 (Sort)----------------------------------------");

            students.Sort();
            for (var i = 0; i < students.Count; i++)
                Console.WriteLine(students[i]);

            Console.WriteLine(
                "---------------------------------------------Task 12_3 (Sort Compare by Cours)----------------------------------------");

            foreach (var cours in students)
            {
                students.Sort(cours.Compare);
                Console.WriteLine(cours);
            }

            Console.WriteLine(
                "---------------------------------------------Task 12_4 (Again Clone)----------------------------------------");

            for (var i = 0; i < students.Count; i++)
            {
                var cloneTruper = (Student) students[i].Clone();
                Console.WriteLine("Атака клонов - " + cloneTruper.StudentName);
            }

            Console.WriteLine(
                "---------------------------------------------Task 12_5 (Dispose)----------------------------------------");

            Console.WriteLine(GC.GetTotalMemory(true));
            var person = new Person("Lary", "Jorge");

            using (person)
            {
            }

            Console.WriteLine(
                "---------------------------------------------Task 12_6 (контейнер Persons)----------------------------------------");
            var collectStudents = Person.CollectStudents(students);
            var count =1;
            foreach (var student in collectStudents)
            {
               
              Console.WriteLine("№{0}. {1}",count,student);
               count++;
               
            }


            Console.ReadLine();

        }
    }
}