﻿using System;
using System.Collections.Generic;

namespace Task_5_1
{
    public class Person : ICloneable, IDisposable, IPrintable
    {
        public Person(string studentName, string teacherName)
        {
            StudentName = studentName;
        }

        public Person(string teacherName)
        {
            TeacherName = teacherName;
        }

        public Person(int teacherName)
        {
            
        }
        
        private Person()
        {
            StudentName = StudentName;
            TeacherName = TeacherName;
        }

        public string StudentName { get; set; }
        public string TeacherName { get; set; }

        public virtual object Clone()
        {
            return new Person {StudentName = StudentName, TeacherName = TeacherName};
        }


        //Task 12_5
        public void Dispose()
        {
            Console.WriteLine("Юзается Dispose?!");
            Console.WriteLine("\nSize used mamory: " + GC.GetTotalMemory(false));
        }

        //Task 12_1
        void IPrintable.Print()
        {
            Console.WriteLine("-\nIPrintable");
            Console.WriteLine("Name: {0}\n", StudentName);
        }

        public virtual void Print()
        {
            Console.WriteLine("Student: {0}\n", StudentName);
        }

        public override string ToString()
        {
            return "";
        }

        public override bool Equals(object obj)
        {
            if ((obj != null) || (GetType() == obj.GetType()))
            {
                var tempPerson = (Person) obj;
                if ((StudentName == tempPerson.StudentName) && (TeacherName == tempPerson.TeacherName) &&
                    (GetHashCode() == tempPerson.GetHashCode()))
                    return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }


        public static void TestToString(Teacher[] teacher, Student[] student)
        {
            for (var i = 0; i < 3; i++)
            {
                Console.WriteLine(teacher[i].ToString());
                Console.WriteLine(student[i].ToString());
            }
        }

        public static void TestEquals<T, Q>(T student, Q teacher)
            where T : class where Q : class
        {
            if (teacher.Equals(student))
                Console.WriteLine("\nОбъект = Объекту");
            else
                Console.WriteLine("\nОбъект != Объект");
        }

        public static void TestGetHashCode<T, V>(T student, V teacher)
            where T : class where V : class
        {
            Console.WriteLine("HashCode {0} object = {1}\nHashCode {2} object = {3}\n", student.GetType().Name,
                student.GetHashCode(), teacher.GetType().Name, teacher.GetHashCode());
            if (student.GetHashCode() == teacher.GetHashCode())
                Console.WriteLine("Равны");
            else
                Console.WriteLine("Не равны");
        }

        public static void GetType<T, Q>(T[] students, Q[] teachers)
            where T : class where Q : class
        {
            var countStudent = 0;
            var countTeacher = 0;
            var studentList = new List<Student>();
            foreach (var item in students)
                if (item is Student)
                {
                    countStudent++;
                    var student = item as Student;
                    if (student.Cours + 1 <= 7)
                        student.Cours++;
                    studentList.Add(student);
                }
            foreach (var item in teachers)
                if (item is Teacher)
                    countTeacher++;
            Console.WriteLine("Eventually learns to magic: {0} Students\nAnd who teach thay: {1} Professors",
                countStudent, countTeacher);
            Console.WriteLine("Students up course:\n");
            foreach (var student in studentList)
                student.Print();
        }

        //Task 12_6
        public static Persons CollectStudents(IEnumerable<Student> students)
        {
            var customStudent = new Persons();
            foreach (var student in students)
            {
               customStudent.Add(student);
            }
            return customStudent;
        }
    }

    public interface IPrintable
    {
        void Print();
    }
}