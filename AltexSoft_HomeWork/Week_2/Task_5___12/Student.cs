﻿using System;
using System.Collections.Generic;

namespace Task_5_1
{
    public sealed class Student : Person, IComparable<Student>, IComparer<Student>, ICloneable

    {
        public Student(string studentName, byte cours, int teacher) : base(studentName, null)
        {
            Creation = teacher;
            Cours = cours;
        }

        public byte Cours { get; set; }
        public int Creation { get; set; }

        //Task 12_4
        public override object Clone()
        {
            return MemberwiseClone();
        }

        //task 12_2
        public int CompareTo(Student student)
        {
            return string.Compare(StudentName, student.StudentName, StringComparison.Ordinal);
        }

        //task 12_3
        public int Compare(Student s1, Student s2)
        {
            return s2.Cours.CompareTo(s1.Cours);
        }

        public new void Print()
        {
            Console.WriteLine("Student: {0}  -  Cours: {1}", StudentName, Cours);
        }

        public override string ToString()
        {
            return string.Format(base.ToString() + "Student: {0} - Cours: {1}", StudentName, Cours);
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj))
            {
                var tempStudent = (Student) obj;
                if ((tempStudent != null) && (Cours == tempStudent.Cours))
                    return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }


        public static void RandomStudent(Student[] someRandomStudent)
        {
            var rand = new Random();
            var tempStudent = rand.Next(0, someRandomStudent.Length);
            someRandomStudent[tempStudent].Print();
        }

        public static void Parents<T>(T someObj) where T : class
        {
            var i = someObj.GetType();

            foreach (var type in i.Assembly.GetTypes())
                if (type.Name == someObj.GetType().Name)
                {
                    Console.WriteLine("{0} Parents: ", type.Name);
                    var derived = type;
                    derived = derived.BaseType;
                    if (derived != null) Console.WriteLine("{0}", derived.Name);

                    Console.WriteLine();
                }
        }
    }
}