﻿using System;
using System.Collections.Generic;

namespace Task_5_1
{
    public class Teacher : Person
    {
        private readonly List<Student> _groupStudents = new List<Student>();
        public Teacher(string teacherName, int id) : base(teacherName)
        {
            Id = id;
        }

        private int Id { get; set; }

        public static void PrintStudents(Student[] students,Teacher[] teachers)
        {
            foreach (var teacher in teachers)
            {
                Console.WriteLine($"\nProfessor: {teacher.TeacherName}");
                foreach (var student in students)
                {
                    if (student.Creation == teacher.Id)
                    {
                        Console.WriteLine(student.ToString());
                        teacher._groupStudents.Add(student);
                    }
                }
            }
        }

        public override void Print()
        {
            Console.WriteLine("Professor: {0}\n", TeacherName);
        }

        public override string ToString()
        {
            return string.Format(base.ToString() + "\nProfessor: {0}", TeacherName);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public static void RandomProff(Teacher[] randProff)
        {
            var rand = new Random();
            var luckyProff = rand.Next(0, randProff.Length);
            randProff[luckyProff].Print();
        }
    }
}