﻿using System;
using System.Collections.Generic;

namespace Task_6
{
    internal class AddCatalog
    {
        public static readonly List<CD> CDList = new List<CD>();
        public static string CatalogName { get; set; }

        public static void Create(string catalogName)
        {
            CatalogName = catalogName;
        }

        public static void GetCatalog()
        {
            Console.WriteLine(CatalogName);
        }

         public static void ShowMuzicCatalog()
        {
            Console.WriteLine("Все что есть: "+ CatalogName);
            foreach (var cd in CDList)
            {
               Console.WriteLine(cd.CDName);
                foreach (var song in CD.SingList)
                {
                        Console.WriteLine("Песня {0}\n Певец {1}", song.NameSong, song.Singer);    
                }
            }
        }

        public virtual void Print()
        {
            Console.WriteLine("Catalog {0}", CatalogName);
        }

        public override string ToString()
        {
            return string.Format("Play List: {0}", CatalogName);
        }
    }
}