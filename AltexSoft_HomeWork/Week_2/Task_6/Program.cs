﻿using System;

namespace Task_6
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Забацаем каталог песенок");

            var playList = Console.ReadLine();
            AddCatalog.Create(playList);

            //Add CD 

            Console.WriteLine("Сколько дисков вы хотите добавить? Введите количество: ");
            byte countDisk = 0;
            byte currentCDID = 0;
            byte maxCDadd = Convert.ToByte(Console.ReadLine());
          
            do
            {
                Console.WriteLine("\nДобавте диск в каталог: ");
                var addCDname = Console.ReadLine();
                var id = currentCDID++;
                Console.WriteLine("Название диска - " + addCDname);
                CD.AddCD(addCDname, id);
                countDisk++;
            } while (countDisk < maxCDadd);
           
            //Add song!
                
            Console.WriteLine("\nВведите количестов добавляемых песен");
            byte countAdd = 0;
            byte curentId = 0;
            var maxSongs = Convert.ToByte(Console.ReadLine());
            do
            {
                Console.WriteLine("На какой диск добаить песню?");
                byte onCdAdd = Convert.ToByte(Console.ReadLine());
                if (onCdAdd == currentCDID)
                {
                    Console.WriteLine("\nДобавте песенку на диск: ");
                    var singer = Console.ReadLine();
                    var nameSong = Console.ReadLine();
                    var id = curentId++;
                    Console.WriteLine("\nSinger: " + singer + "\nSong: " + nameSong + "\nID " + id);
                    Song.AddSong(singer, nameSong, id);
                    countAdd++;
                }
                else
                {
                    Console.WriteLine("Не тот диск?");
                }
                
            } while (countAdd < maxSongs);

            
            Song.DelSong();
            CD.DelCD();
            AddCatalog.ShowMuzicCatalog();
            

            Song.SearchSong();

            //Console.WriteLine("\nВ итоге песенок:\n ");
            //Song.ShowAlbum();
            //CD.ShowAllCD();
            //CD.ShowOnCD();


            Console.ReadKey();
        }
    }
}