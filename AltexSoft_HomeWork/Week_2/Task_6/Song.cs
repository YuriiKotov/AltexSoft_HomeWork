﻿using System;

namespace Task_6
{
    internal class Song : CD
    {
        public Song(string singer, string nameSong, byte id) : base(null, 0)
        {
            Singer = singer;
            NameSong = nameSong;
            Id = id;
        }

        public string Singer { get; set; }
        public string NameSong { get; set; }
        public byte Id { set; get; }

        public static void ShowAlbum()
        {
            foreach (var song in SingList)
                Console.WriteLine(song.ToString());
        }

        public static void AddSong(string singer, string nameSong, byte id)
        {
            SingList.Add(new Song(singer, nameSong, id));
        }


        public static void DelSong()
        {
            ShowAlbum();
            byte delSong = 1;
            Console.WriteLine("\nМожете удалить песню!\n");
            Console.WriteLine("Точно хотите удалать? Если да нажмите - 1; Если нет - 2\n");

            var chois = Convert.ToByte(Console.ReadLine());
            if (delSong == chois)
            {
                Console.WriteLine("\nВыберите удаляемую песню: ");
                var curent = Convert.ToByte(Console.ReadLine());
                foreach (var song in SingList.ToArray())
                    if (song.Id == curent)
                    {
                        SingList.Remove(song);
                        Console.WriteLine("\nSucsses");
                        ShowAlbum();
                    }
            }
        }
        public static void SearchSong()
        {
            Console.WriteLine("Поищем хиты? Введите название хита: ");
            string findHit = Console.ReadLine();
            foreach (var song in SingList)
            {
                if (song.NameSong == findHit )
                {
                    Console.WriteLine("Хит - {0}\nИсполнитель - {1}",song.NameSong,song.Singer);
                }
            }
        }

        public override string ToString()
        {
            return $"Певец: {Singer}  песня: {NameSong}";
        }
    }
}