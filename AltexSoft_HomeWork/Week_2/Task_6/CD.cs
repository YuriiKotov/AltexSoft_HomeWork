﻿using System;
using System.Collections.Generic;

namespace Task_6
{
    internal class CD : AddCatalog
    {
        public static readonly List<Song> SingList = new List<Song>();

        public CD(string cdName, byte cDID)
        {
            CDName = cdName;
            CDID = cDID;
        }

        public string CDName { get; set; }
        public byte CDID { get; set; }

        public static void AddCD(string cdName, byte CDID)
        {

            CDList.Add(new CD(cdName, CDID));
        }

        public static void ShowAllCD()
        {
            foreach (var disk in CDList)
                Console.WriteLine(disk.ToString());
        }

        public static void ShowOnCD()
        {
            foreach (var disk in CDList)
            {
                Console.WriteLine(disk.ToString());
                foreach (var onDisk in SingList)
                    Console.WriteLine(onDisk.ToString());
            }
        }

        public static void DelCD()
        {
            ShowAllCD();
            byte delCD = 1;
            Console.WriteLine("Можете удалить диск!");
            Console.WriteLine("Точно хотите удалать? Если да нажмите - 1; Если нет - 2");
            var chois = Convert.ToByte(Console.ReadLine());
            if (chois == delCD)
            {
                Console.WriteLine("Выберите удаляемый CD: ");
                var curent = Convert.ToByte(Console.ReadLine());
                foreach (var disk in CDList.ToArray())
                    if (disk.CDID == curent)
                    {
                        CDList.Remove(disk);
                        SingList.Clear();
                        Console.WriteLine("Sucsses");
                    }
            }
        }

        public override string ToString()
        {
            return $"Диск - {CDName}\n";
        }
    }
}