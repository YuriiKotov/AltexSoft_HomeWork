﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_8_2
{
    class UnManage
    {
        readonly string _text = @"H:\c#\AltexSoft\AltexSoft_HomeWork\AltexSoft_HomeWork\Week_2\Task_8_2\UnManageFirst.txt";

        public virtual void Print()
        {
            using (StreamReader readUnManager = new StreamReader(_text, System.Text.Encoding.Default))
            {
                string line;
                Console.WriteLine();
                while ((line = readUnManager.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
                readUnManager.Close();
                Console.WriteLine("\nSize used mamory: " + GC.GetTotalMemory(false));
            }
        }

        ~UnManage()
        {
           
            Console.ForegroundColor = ConsoleColor.Yellow;
            GC.Collect();
            Console.WriteLine("\nОбъект уничтожен");
            Console.WriteLine("\nSize used mamory: " + GC.GetTotalMemory(false));
            Console.ReadKey();
        }
    }
}
