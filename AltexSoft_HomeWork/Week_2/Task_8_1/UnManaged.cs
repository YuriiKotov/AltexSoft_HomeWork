﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Task_8_1
{
    class UnManaged:Managed
    {
        //private static readonly string Way = Directory.GetCurrentDirectory();
        readonly string _text = @"H:\c#\AltexSoft\AltexSoft_HomeWork\AltexSoft_HomeWork\Week_2\Task_8_1\file.txt";

        public override void Print()
        {
            using (StreamReader readUnManager = new StreamReader(_text, System.Text.Encoding.Default))
            {
                string line;
                Console.WriteLine();
                while ((line = readUnManager.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
                readUnManager.Close();
                Console.WriteLine("\nSize used mamory: " + GC.GetTotalMemory(false));
                //GC.Collect();
            }
            
        }

        ~UnManaged()
        {
            Console.Beep();
            Console.WriteLine("\nSize used mamory: " + GC.GetTotalMemory(false));
            Console.ReadKey();
        }
    }
}
