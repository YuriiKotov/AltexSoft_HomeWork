﻿using System;

namespace Task_8_1
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var useManaged = new Managed();

            useManaged.Print();
            Console.ReadKey();

            var useUnManager = new UnManaged();
            //GC.Collect();


            useUnManager.Print();
          
           // useUnManager.Dispose();
            Console.WriteLine("\nSize used mamory: " + GC.GetTotalMemory(false));

            Console.ReadKey();
        }
    }
}