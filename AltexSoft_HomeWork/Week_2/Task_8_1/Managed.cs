﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_8_1
{
    class Managed
    {
        public virtual void Print()
        {
            Console.WriteLine("Size used mamory: " + GC.GetTotalMemory(false));

            Console.WriteLine("\nTest - useing 50000 empty objects: \n");
           object[] managed = new object[50000];

            for (int i = 0; i < managed.Length; i++)
            {
                managed[i] = new object();
            }
            Console.WriteLine("Size used mamory: " + GC.GetTotalMemory(false));
        }
    }
}
