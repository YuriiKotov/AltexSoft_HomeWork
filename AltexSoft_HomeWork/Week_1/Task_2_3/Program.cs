﻿using System;

namespace ConsoleApplication1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Введите коичество строк: ");
            var N = Convert.ToInt32(Console.ReadLine());
            var someNumbs = new int[N];
            var someRandom = new Random();

            for (var i = 0; i < N; i++)
                someNumbs[i] = someRandom.Next(-500, 500);

            for (var i = 0; i < N; i++)
            {
                Console.Write(someNumbs[i] + " ");
                Console.WriteLine();
            }

            Console.ReadKey();

            int sort;
            for (var i = 0; i < someNumbs.Length - 1; i++)
                for (var j = i + 1; j < someNumbs.Length; j++)
                    if (someNumbs[i] > someNumbs[j])
                    {
                        sort = someNumbs[i];
                        someNumbs[i] = someNumbs[j];
                        someNumbs[j] = sort;
                    }
            Console.WriteLine(" ");
            Console.WriteLine("Вывод отсортированного массива");
            for (var i = 0; i < someNumbs.Length; i++)
                Console.WriteLine(someNumbs[i]);
            Console.ReadLine();
        }
    }
}