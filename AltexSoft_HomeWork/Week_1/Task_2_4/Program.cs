﻿using System;

namespace Task_2_4
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Введите количество строк: ");
            var N = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите количество столбцов: ");
            var M = Convert.ToInt32(Console.ReadLine());
            var someArrayNumbs = new int[N, M];
            var rand = new Random();
            var unsorted = new int[M];
            var sorted = new int[M];

            

            for (var i = 0; i < N; i++)
                for (var j = 0; j < M; j++)
                    someArrayNumbs[i, j] = rand.Next(-500, 500);

            Console.WriteLine("То что получили: ");

            for (var i = 0; i < N; i++)
            {
                for (var j = 0; j < M; j++)
                    Console.Write(someArrayNumbs[i, j] + "   ");
                Console.WriteLine();
            }

            var summ = 0;
            

            for (int i = 0; i < M; i++)
            { 
                for (int j = 0; j < N; j++)
                {
                    summ += someArrayNumbs[j, i];

                }
                unsorted[i] = summ;
                sorted[i] = summ;
            }

            Console.WriteLine();
            Array.Sort(sorted);

            foreach (int sort in sorted)
            {
                Console.WriteLine("Сортирован: " + sort);
            }
            foreach (int sort in unsorted)
            {
                Console.WriteLine("Не сортированый: " + sort);
            }

            /* for (int i = 0; i < sorted.Length -1; i++)
             {
                 for (int j =i +1; j < unsorted.Length; j++)
                 {
                     if (sorted[i]<unsorted[i])
                     {
                         var buf = sorted[i];
                         sorted[i] = unsorted[i];
                         unsorted[i] = buf;
                     }
                 }
             }*/
         Console.ReadKey();
        }
    }
}