﻿using System;
using System.Collections.Generic;

namespace Task_4_3
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            List<WorkedSaveCamer> passengers = new List<WorkedSaveCamer>();
            passengers.Add(new SaveCamer("Чебурашка", 2, 30));
            passengers.Add(new SaveCamer("Крокодил Гена", 4, 43));
            passengers.Add(new SaveCamer("Шапокляк", 1, 5));
            passengers.Add(new SaveCamer("Доктор Кто", 1, 2));
            passengers.Add(new SaveCamer("Амун Ра", 5, 23));
            passengers.Add(new SaveCamer("Наруто", 2, 13));

            foreach (var pass in passengers)
                pass.Print();

            var maxWeight = 50;

            Console.WriteLine("Максимальный средний вес багажа: " + maxWeight);
            Console.WriteLine();

            int countWeight;
            passengers.Sort();

            foreach (var passenger  in passengers)
            {
                countWeight = passenger.CountBags*passenger.WeightBegs;
                if (countWeight >= maxWeight)
                    Console.WriteLine(passenger.NamePass + ": количестов багажа - " + passenger.CountBags +
                                      " - средний вес - " + countWeight);
            }
            Console.ReadKey();
        }
    }
}