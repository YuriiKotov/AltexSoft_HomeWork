﻿using System;

namespace Task_4_3
{
    internal class WorkedSaveCamer
    {
        public WorkedSaveCamer(string namePass, byte countBags, byte weightBegs)
        {
            NamePass = namePass;
            CountBags = countBags;
            WeightBegs = weightBegs;
        }

        public string NamePass { get; set; }
        public byte CountBags { get; set; }
        public byte WeightBegs { get; set; }

        public void Print()
        {
            Console.WriteLine("Имя пассажира: " + NamePass + "; \n" + "Количество багажа: " + CountBags + ";\n" +
                              "Вес багажа (кг/шт): " + WeightBegs);
            Console.WriteLine();
        }
    }
}