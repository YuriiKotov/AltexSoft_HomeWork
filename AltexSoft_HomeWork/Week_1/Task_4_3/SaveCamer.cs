﻿using System;

namespace Task_4_3
{
    internal class SaveCamer : WorkedSaveCamer, IComparable
    {
        public SaveCamer(string namePass, byte countBegs, byte weightBegs)
            : base(namePass, countBegs, weightBegs)
        {
        }

        public int CompareTo(object obj)
        {
            var inCamer = (SaveCamer) obj;

            if (CountBags > inCamer.CountBags) return 1;
            if (CountBags < inCamer.CountBags) return -1;

            return 0;
        }
    }
}