﻿using System;
using System.Collections.Generic;

namespace Task_4_2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var students = new List<StudentList>();
            students.Add(new Student("Лея Амидала", 2, true, true, true));
            students.Add(new Student("mr Nobody", 1, false, false, true));
            students.Add(new Student("Чубака", 2, true, true, false));
            students.Add(new Student("Энакин Скайвокер", 1, true, true, true));
            students.Add(new Student("Мейс Винду", 1, true, true, true));
            students.Add(new Student("Троль из под Моста", 3, true, false, true));
            students.Add(new Student("Питер Пен", 3, true, true, true));
            students.Add(new Student("Воланд", 3, true, true, true));
            students.Add(new Student("Люк Скайвокер", 2, true, true, true));
            students.Add(new Student("Маленький Принц", 1, true, false, false));
            students.Add(new Student("Гарри Поттер", 3, true, true, true));
            students.Add(new Student("Джон Сноу", 2, false, false, false));
            students.Add(new Student("Коровьев", 3, true, false, true));

            students.Sort();

            foreach (var student in students)
                if (student.Chemestry && student.Mathmatic && student.History)
                {
                    student.Print();
                    Console.WriteLine();
                }

            Console.ReadLine();
        }
    }
}