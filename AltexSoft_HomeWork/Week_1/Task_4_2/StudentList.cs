﻿using System;

namespace Task_4_2
{
    internal class StudentList
    {
        public StudentList(string fio, byte classGroup, bool mathematic, bool chemestry, bool history)
        {
            Fio = fio;
            ClassGroup = classGroup;
            Mathmatic = mathematic;
            Chemestry = chemestry;
            History = history;
        }

        public string Fio { get; set; }
        public byte ClassGroup { get; set; }
        public bool Mathmatic { get; set; }
        public bool Chemestry { get; set; }
        public bool History { get; set; }

        public void Print()
        {
            var newString = "\n";
            Console.WriteLine("Имя/Фамилия: " + Fio + ";" + newString + "Группа: " + ClassGroup + ";" + newString
                              + "Математика: " + Mathmatic + ";" + newString + "Химия: " + Chemestry + ";" + newString
                              + "История: " + History + ";");
        }
    }
}