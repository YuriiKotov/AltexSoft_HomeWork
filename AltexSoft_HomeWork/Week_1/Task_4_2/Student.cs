﻿using System;

namespace Task_4_2
{
    internal class Student : StudentList, IComparable
    {
        public Student(string fio, byte classGroup, bool mathematic, bool chemestry, bool history)
            : base(fio, classGroup, mathematic, chemestry, history)
        {
        }

        public int CompareTo(object obj)
        {
            var student = (Student) obj;

            if (ClassGroup > student.ClassGroup) return 1;
            if (ClassGroup < student.ClassGroup) return -1;

            return 0;
        }
    }
}