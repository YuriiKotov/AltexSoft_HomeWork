﻿using System;

namespace Students
{
    public class StudentStructur
    {
        public struct Students : IComparable
        {
            public string Fio { get; set; }
            public byte Age { get; set; }

            public Students(string fio, byte age)
            {
                 this.Age = age;
                 this.Fio = fio;
            }

            public int CompareTo(object obj)
            {
                var student = (Students) obj;

                if (Age > student.Age) return 1;
                if (Age < student.Age) return -1;

                return 0;
            }
        }
    }
}