﻿using System;
using System.Collections.Generic;

namespace Students
{
    internal class Program
    {
        public struct Students : IComparable
        {
            public string Fio { get; set; }
            public byte Age { get; set; }

            public Students(string fio, byte age)
            {
                Age = age;
                Fio = fio;
            }

            public void Print()
            {
                Console.WriteLine("Имя: {0}; \nВозраст: {1}\n", Fio,Age);
            }

            public int CompareTo(object obj)
            {
                var student = (Students)obj;

                if (Age > student.Age) return 1;
                if (Age < student.Age) return -1;

                return 0;
            }
        }

        public static void Main(string[] args)
        {
           
            List<Students> someArrayStudents = new List<Students>();

            someArrayStudents.Add(new Students("Павлик Морозов", 25));
            someArrayStudents.Add(new Students("Алексаедр Македонский", 18));
            someArrayStudents.Add(new Students("Рональд МкДональ", 43));
            someArrayStudents.Add(new Students("Тот кого нелья называть", 53));
            someArrayStudents.Add(new Students("Гомер Симпсон", 42));
            someArrayStudents.Add(new Students("Лиса Симпсон",8));
            
            someArrayStudents.Sort();

            foreach (var student in someArrayStudents)
            {
                student.Print();
            }
           

            Console.ReadKey();
        }
    }
}