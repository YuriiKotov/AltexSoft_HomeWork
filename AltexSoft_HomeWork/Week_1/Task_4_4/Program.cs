﻿using System;
using System.Collections.Generic;

namespace Task_4_4
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var autoList = new List<WorkWithAutoList>();
            autoList.Add(new Auto("Porshe", "AA1128", "Barry Alen", 2014, 125000));
            autoList.Add(new Auto("Aston Martin", "1244Ll", "Batman", 2013, 15000));
            autoList.Add(new Auto("Bently", "AA0000", "Georg Martin", 2005, 305000));
            autoList.Add(new Auto("Чайка", "ПП0000", "Брежнев", 1968, 5000));
            autoList.Add(new Auto("Копейка", "AA11ХА", "Вася Пупкин", 1961, 2));

            autoList.Sort();

            Console.WriteLine("Введите заданый год покупки: ");
            Console.WriteLine("P.S.: с 1961 до 2014");
            Console.WriteLine();

            var dataBuy = Convert.ToInt32(Console.ReadLine());
            foreach (var auto in autoList)
                if (auto.BuyYear <= dataBuy)
                {
                    auto.Print();
                    Console.WriteLine();
                }
            Console.ReadKey();
        }
    }
}