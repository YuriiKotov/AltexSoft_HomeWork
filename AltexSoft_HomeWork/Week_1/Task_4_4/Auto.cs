﻿using System;

namespace Task_4_4
{
    internal class Auto : WorkWithAutoList, IComparable
    {
        public Auto(string autoMark, string numberAuto,
            string nameDriver, short buyYear, int distanceMove)
            : base(autoMark, numberAuto, nameDriver, buyYear, distanceMove)
        {
        }

        public int CompareTo(object obj)
        {
            var distance = (Auto) obj;

            if (DistanceMove > distance.DistanceMove) return 1;
            if (DistanceMove < distance.DistanceMove) return -1;

            return 0;
        }
    }
}