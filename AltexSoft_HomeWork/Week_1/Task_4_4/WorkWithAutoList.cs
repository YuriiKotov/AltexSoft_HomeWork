﻿using System;

namespace Task_4_4
{
    internal class WorkWithAutoList
    {
        public WorkWithAutoList(string autoMark, string numberAuto,
            string nameDriver, short buyYear, int distanceMove)
        {
            AutoMark = autoMark;
            NumberAuto = numberAuto;
            NameDriver = nameDriver;
            BuyYear = buyYear;
            DistanceMove = distanceMove;
        }

        public string AutoMark { get; set; }
        public string NumberAuto { get; set; }
        public string NameDriver { get; set; }
        public short BuyYear { get; set; }
        public int DistanceMove { get; set; }

        public void Print()
        {
            Console.WriteLine("Автомобиль: " + AutoMark + ";\n" + "Номер машины: " + NumberAuto + "; \n" +
                              "Имя владельца: " + NameDriver + ";\n" + "Год покупки: " + BuyYear + ";\n" +
                              "Пройденная дистанция: " + DistanceMove);
        }
    }
}