﻿using System;

namespace Task_4_1
{
    public class Student : GruopStudents, IComparable
    {
        public Student(string studentName, int studentBornYear, string studentHomeAdress, byte studentWhatSchoolName)
            : base(studentName, studentBornYear, studentHomeAdress, studentWhatSchoolName)
        {
        }

        public int CompareTo(object obj)
        {
            var student = (Student) obj;

            if (StudentBornYear > student.StudentBornYear) return 1;
            if (StudentBornYear < student.StudentBornYear) return -1;

            return 0;
        }
    }
}