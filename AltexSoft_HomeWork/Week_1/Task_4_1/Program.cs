﻿using System;
using System.Collections.Generic;
using ConsoleApplication3;

namespace Task_4_1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var students = new List<GruopStudents>();

            students.Add(new Student("Лея Амидала", 3000, "Nabu, castel of Goverment", 15));
            students.Add(new Student("Энакин Скайвокер", 2920, "Tatooin, slave barack", 10));
            students.Add(new Student("mr Nobody", 1900, "Some where", 15));
            students.Add(new Student("Троль из под Моста", 1990, "New York, Central Park, bridg", 47));
            students.Add(new Student("Маленький Принц", 1750, "Small planet", 47));
            students.Add(new Student("Гарри Поттер", 1980, "Little Uinghet, Privet Drive 4", 10));
            students.Add(new Student("Джон Сноу", 283, "The Wall", 15));


            Console.WriteLine("Укажите по какой школе вести поиск: ");
            Console.WriteLine("П.С.: №15, №10, №47");
            var searchStudents = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine();

            students.Sort();

            foreach (var student in students)
                if (student.StudentWhatSchoolName == searchStudents)
                {
                    student.Print();
                    Console.WriteLine();
                }

            GruopStudents someStudent = students[1];
            Serialaze.XMLSerialize(someStudent);
            var someStudentDes = Serialaze.XMLDeserialize();
            Console.WriteLine(someStudentDes);

           // Serialaze.BinarSerialize(someNew);
            //Serialaze.SOAPSerialaze(someNew);
            

            Console.ReadKey();
        }
    }
}