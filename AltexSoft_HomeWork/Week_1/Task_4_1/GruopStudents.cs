﻿using System;

namespace Task_4_1
{
    [Serializable]
    public class GruopStudents
    {
        private string StudentName { get; }
        protected int StudentBornYear { get; }
        private string StudentHomeAdress { get; }
        public byte StudentWhatSchoolName { get; }

        protected GruopStudents(string studentName, int studentBornYear, string studentHomeAdress,
            byte studentWhatSchoolName)
        {
            StudentName = studentName;
            StudentBornYear = studentBornYear;
            StudentHomeAdress = studentHomeAdress;
            StudentWhatSchoolName = studentWhatSchoolName;
        }

        public GruopStudents()
        {
            
        }

        public void Print()
        {
            var newString = "\n";
            Console.WriteLine("Имя/Фамилия: " + StudentName + ";" + newString + "год рождения " + StudentBornYear
                              + ";" + newString + "Домашний адресс: " + StudentHomeAdress + ";" + newString
                              + "Школа №" + StudentWhatSchoolName + ";");
        }
    }
}