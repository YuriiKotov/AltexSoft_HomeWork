﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Xml.Serialization;
using Task_4_1;

namespace ConsoleApplication3
{
     static class Serialaze 
    {
        public static void XMLSerialize(GruopStudents student)
        {         
            using (var streamReader = new FileStream(@"test.xml", FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                var ser = new XmlSerializer(typeof(GruopStudents));
                ser.Serialize(streamReader, student);
            }
        }
    
        public static GruopStudents XMLDeserialize()
        {
            var ser = new XmlSerializer(typeof(GruopStudents));
            GruopStudents students;
            using (
                var streamReader = new FileStream(@"test.xml", FileMode.OpenOrCreate,
                    FileAccess.Read, FileShare.Read))
            {
                students = (GruopStudents) ser.Deserialize(streamReader);
            }
            return students;
        }
        

        public static void BinarSerialize(GruopStudents student)
        {
            using (var streamReader = new FileStream("Serizalie.dat", FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read)
            )
            {
                var ser = new BinaryFormatter();
                ser.Serialize(streamReader, student);
            }
        }

        public static void SOAPSerialaze(GruopStudents student)
        {
            using (
                var streamReader = new FileStream("Serizalie.soap", FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
            {
                var ser = new SoapFormatter();
                ser.Serialize(streamReader, student);
            }
        }
    }
}