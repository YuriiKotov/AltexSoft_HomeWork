﻿using System;
using System.IO;

namespace Task_3_3
{
    internal static class Program
    {
        private static readonly string OS = Environment.OSVersion.ToString().ToLower();
        private static string var => OS.Contains("Windows 10") ? "/" : "\\";

        private static string UpTop(string way, int curentStapUp)
        {
            for (var i = 0; i < curentStapUp; i++)
                way = way.Substring(0, way.LastIndexOf(var, StringComparison.Ordinal));
            return way;
        }

        private static void Dir(string fullWay, string offset = " ")
        {
            foreach (var ways in Directory.GetDirectories(fullWay))
            {
                Console.WriteLine(ways.Replace(fullWay, offset));

                foreach (var files in Directory.GetFiles(ways))
                    Console.WriteLine(files.Replace(ways + var, offset + "~~"));
                Dir(ways, offset + "  ");
            }
        }

        private static void Main(string[] args)
        {
            var way = Directory.GetCurrentDirectory();
            way = UpTop(way, 3);
            Dir(way);

            Console.WriteLine("\n\nWanna esc? click any button!");
            Console.ReadKey();
        }
    }
}