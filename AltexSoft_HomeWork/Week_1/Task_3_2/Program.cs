﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Task_3_2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var way = Directory.GetCurrentDirectory();

            var filePlace = way + @"someNumbs.txt";
            var regExp = @"[0-9]+";
            var numbs = "2 4 16 256 5 55 61 101";
            var temp = "";

            foreach (Match matches in Regex.Matches(numbs, regExp))
                temp += int.Parse(matches.Value)*int.Parse(matches.Value) + " ";

            Console.WriteLine("Числа к возведеню в квадрат: " + numbs);
            File.WriteAllText(filePlace, temp, Encoding.UTF8);
            Console.WriteLine("Результат возведения в квадрат: " + File.ReadAllText(filePlace));

            Console.ReadKey();
        }
    }
}