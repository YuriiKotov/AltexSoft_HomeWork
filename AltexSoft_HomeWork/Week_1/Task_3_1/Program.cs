﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Task_3_1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var text =
                File.ReadAllText(@"H:\c#\AltexSoft\AltexSoft_HomeWork\AltexSoft_HomeWork\Week_1\Task_3_1\numbrs.txt");
            var regExpr = @"-?[0-9]+(.|,)[0-9]";

            Console.WriteLine(text);
            Console.ReadKey();

            double sum = 0;

            foreach (Match matches in Regex.Matches(text, regExpr))
                sum += Convert.ToSingle(matches.Value.Replace(".", ","));

            Console.WriteLine("\n" + sum);
            Console.ReadKey();
        }
    }
}