﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Task_13
{
    public delegate int SortedList<in T>(T x, T y);

    internal class Program
    {
      static void Main(string[] args)
        {
           var someArray = new List<int>() {1,5,3,2,0,1,-2,-15,65};
           var sort = someArray.ExtendSort(new ToCompare());
            foreach (var i in sort)
            {
                Console.WriteLine("First - {0}",i);
            }

            var sortVDlegat = new SortedList<int>(Sorting.Sort);
            var sortV2 = someArray.ExtendWithDelegat(sortVDlegat);
            foreach (var i in someArray)
            {
                Console.WriteLine("Second - {0}",i);
            }

           
            Console.ReadKey();
        }
    }
}