﻿using System;
using System.Collections.Generic;

namespace Task_13
{
    internal static class Sorting
    {
        public static List<T> ExtendSort<T>(this IEnumerable<T> enumerable, ICompare Compare)
        {
            var array = (List<T>) enumerable;
            array.Sort(Compare.CompareInt);
            return array;
        }

        public static List<T> ExtendWithDelegat<T>(this IEnumerable<T> enumerable, SortedList<T> delegat)
        {
            var array = (List<T>) enumerable;
            array.Sort(delegat.Invoke);
            return array;
        }
        public static int Sort<T>(T obj1, T obj2)
        {
            return string.Compare(obj1.ToString(), obj2.ToString(), StringComparison.Ordinal);
        }
    }

    internal class ToCompare : ICompare
    {
        int ICompare.CompareInt<T>(T obj1, T obj2)
        {
            return string.Compare(obj1.ToString(), obj2.ToString(), StringComparison.Ordinal);
        }
    }

    internal interface ICompare
    {
        int CompareInt<T>(T obj1, T obj2);
    }
}