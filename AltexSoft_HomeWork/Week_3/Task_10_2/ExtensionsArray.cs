﻿using System.Collections;
using System.Collections.Generic;


namespace Task_10_2
{
    internal static class ArrayListExtension
    {
       
        public static ArrayList BySelectSorts(this ArrayList arrayList)
        {
            for (var i = 0; i < arrayList.Count - 1; i++)
            {
                var min = i;
                for (var j = i + 1; j < arrayList.Count; j++)
                {
                    if ((int)arrayList[j] < (int)arrayList[min])
                    {
                        min = j;
                    }
                }

                if (min == i) continue;
                var temp = (int)arrayList[i];
                arrayList[i] = arrayList[min];
                arrayList[min] = temp;
            }
            return arrayList;
        }

        public static ArrayList ByShakerSort(this ArrayList arrayList)
        {
            var b = 0;
            var left = 0;
            var right = arrayList.Count - 1;
            while (left < right)
            {
                for (var i = left; i < right; i++)
                {
                    if ((int)arrayList[i] <= (int)arrayList[i + 1]) continue;
                    b = (int)arrayList[i];
                    arrayList[i] = arrayList[i + 1];
                    arrayList[i + 1] = b;
                    b = i;
                }
                right = b;
                if (left >= right) break;
                for (var i = right; i > left; i--)
                {
                    if ((int)arrayList[i - 1] <= (int)arrayList[i]) continue;
                    b = (int)arrayList[i];
                    arrayList[i] = (int)arrayList[i - 1];
                    arrayList[i - 1] = b;
                    b = i;
                }
                left = b;
            }
            return arrayList;
        }
    }
}
