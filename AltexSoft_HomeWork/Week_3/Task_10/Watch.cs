﻿using System;
using System.IO;
using System.Security.Permissions;

namespace Task_10
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    internal class Watch
    {
        public static void GoPro(string filter)
        {
            var watcher = new FileSystemWatcher();

            watcher.Path = Directory.GetCurrentDirectory();

            watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            watcher.Filter = "*." + filter;

            watcher.Changed += OnChanged;
            watcher.Created += OnChanged;
            watcher.Deleted += OnChanged;
            watcher.Renamed += OnRenamed;

            watcher.EnableRaisingEvents = true;
            CreateLog(Directory.GetCurrentDirectory() + @"\..\..\");
            Console.WriteLine("Начать просмотр");

            Console.WriteLine("Тыкнуть \'q\' чтобы остановить просмотр!");
            while (Console.Read() != 'q') ;
        }


        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            Console.WriteLine("File: " + e.FullPath + " " + e.ChangeType);
        }

        private static void OnRenamed(object source, RenamedEventArgs e)
        {
            Console.WriteLine("File: {0} renamed to {1}", e.OldFullPath, e.FullPath);
        }

        private static void CreateLog(string path)
        {
            var file = new FileInfo(path + "Test Log.txt");
            if (!file.Exists)
                file.Create().Close();
        }
    }
}