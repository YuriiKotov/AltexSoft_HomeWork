﻿using System;

namespace Task_11
{
    class Program
    {
        static void Main(string[] args)
        {
            var forTest = default(Complex);
            var forTest2 = new Complex(5, 5);
            var forTest3 = new Complex(forTest2);



            Console.WriteLine("ToString - {0}\nEquals - {1}\nGetHeshCode - {2}",
                forTest2.ToString(), forTest2.Equals(forTest3), forTest2.GetHashCode());

            Console.WriteLine(forTest == forTest3);
            Console.WriteLine(forTest != forTest3);

            Console.ReadLine();

        }
    }
}
