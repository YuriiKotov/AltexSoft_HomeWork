﻿using System;

namespace Task_11
{
    struct Complex
    {
        private double _someRealNumb;
        private double _someAbstractNumb;

        public Complex(double a)
        {
            _someRealNumb = a;
            _someAbstractNumb = 0;
        }

        public Complex(double a, double i)
        {
            _someRealNumb = a;
            _someAbstractNumb = i;
        }

        public static Complex operator +(Complex c1, Complex c2)
        {
            return new Complex(c1._someRealNumb + c2._someRealNumb, c1._someAbstractNumb + c2._someAbstractNumb);
        }

        public static Complex operator -(Complex c1, Complex c2)
        {
            return new Complex(c1._someRealNumb - c2._someRealNumb, c1._someAbstractNumb - c2._someAbstractNumb);
        }

        public static Complex operator *(Complex c1, Complex c2)
        {
            return new Complex(c1._someRealNumb * c2._someRealNumb - c1._someAbstractNumb * c2._someAbstractNumb,
                c1._someRealNumb * c2._someAbstractNumb + c1._someAbstractNumb * c2._someRealNumb);
        }

        public static Complex operator /(Complex c1, Complex c2)
        {
            var temp = new Complex();
            var r = c2._someRealNumb * c2._someRealNumb + c2._someAbstractNumb * c2._someAbstractNumb;
            temp._someRealNumb = (c1._someRealNumb * c2._someRealNumb + c1._someAbstractNumb * c2._someAbstractNumb) / r;
            temp._someAbstractNumb = (c1._someAbstractNumb * c2._someRealNumb - c1._someRealNumb * c2._someAbstractNumb) / r;
            return temp;
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || (obj.GetType() != typeof(Complex))) return false;
            var complex = obj as Complex? ?? new Complex();
            return (Math.Abs(_someRealNumb - complex._someRealNumb) < 0.0000001) &&
                   (Math.Abs(_someAbstractNumb - complex._someAbstractNumb) < 0.0000001);
        }

        public bool Equals(Complex other)
        {
            return _someRealNumb.Equals(other._someRealNumb) && _someAbstractNumb.Equals(other._someAbstractNumb);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_someRealNumb.GetHashCode() * 397) ^ _someAbstractNumb.GetHashCode();
            }
        }

        public override string ToString()
        {
            return _someRealNumb + " " + _someAbstractNumb;
        }

        public static bool operator ==(Complex c1, Complex c2)
        {
            if ((Math.Abs(c1._someRealNumb - c2._someRealNumb) < 0.0000001) &&
                (Math.Abs(c1._someAbstractNumb - c2._someAbstractNumb) < 0.0000001))
                return true;
            return false;
        }

        public static bool operator !=(Complex c1, Complex c2)
        {
            return (Math.Abs(c1._someRealNumb - c2._someRealNumb) > 0.0000001) ||
                   (Math.Abs(c1._someAbstractNumb - c2._someAbstractNumb) > 0.0000001);
        }

        public static implicit operator double(Complex c1)
        {
            return c1._someRealNumb;
        }
    }
}
